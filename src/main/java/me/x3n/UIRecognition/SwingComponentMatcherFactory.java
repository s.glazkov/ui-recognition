package me.x3n.UIRecognition;

import javax.swing.*;

public class SwingComponentMatcherFactory extends ComponentMatcherFactory {
    @Override
    protected ComponentMatcher doConstructMatcher(JComponent component) {
        if (component instanceof JButton) {
            return constructButtonMatcher((JButton) component);
        }
        if (component instanceof JRadioButton) {
            return constructRadioButtonMatcher((JRadioButton) component);
        }
        if (component instanceof JCheckBox) {
            return constructCheckBoxMatcher((JCheckBox) component);
        }
        if (component instanceof JLabel) {
            return constructLabelMatcher((JLabel) component);
        }
        if (component instanceof JTextField) {
            return constructTextFieldMatcher((JTextField) component);
        }
        if (component instanceof JTextArea) {
            return constructTextAreaMatcher((JTextArea) component);
        }
        if (component instanceof JEditorPane) {
            return constructEditorPaneMatcher((JEditorPane) component);
        }
        if (component instanceof JComboBox) {
            return constructComboBoxMatcher((JComboBox) component);
        }
        if (component instanceof JList) {
            return constructListMatcher((JList) component);
        }
        if (component instanceof JSpinner) {
            return constructSpinnerMatcher((JSpinner) component);
        }
        if (component instanceof JSlider) {
            return constructSliderMatcher((JSlider) component);
        }
        if (component instanceof JProgressBar) {
            return constructProgressBarMatcher((JProgressBar) component);
        }
        if (component instanceof JScrollBar) {
            return constructScrollBarMatcher((JScrollBar) component);
        }
        return super.doConstructMatcher(component);
    }

    @Override
    public ComponentMatcherFactory clone() {
        return new SwingComponentMatcherFactory();
    }

    protected ComponentMatcher constructButtonMatcher(JButton button) {
        return new ComponentMatcher<>(button);
    }

    protected ComponentMatcher constructRadioButtonMatcher(JRadioButton radioButton) {
        return new ComponentMatcher<>(radioButton);
    }

    protected ComponentMatcher constructCheckBoxMatcher(JCheckBox checkBox) {
        return new ComponentMatcher<>(checkBox);
    }

    protected ComponentMatcher constructLabelMatcher(JLabel label) {
        return new ComponentMatcher<>(label);
    }

    protected ComponentMatcher constructTextFieldMatcher(JTextField textField) {
        return new ComponentMatcher<>(textField);
    }

    protected ComponentMatcher constructTextAreaMatcher(JTextArea textArea) {
        return new ComponentMatcher<>(textArea);
    }

    protected ComponentMatcher constructEditorPaneMatcher(JEditorPane editorPane) {
        return new ComponentMatcher<>(editorPane);
    }

    protected ComponentMatcher constructComboBoxMatcher(JComboBox comboBox) {
        return new ComponentMatcher<>(comboBox);
    }

    protected ComponentMatcher constructListMatcher(JList list) {
        return new ComponentMatcher<>(list);
    }

    protected ComponentMatcher constructSpinnerMatcher(JSpinner spinner) {
        return new ComponentMatcher<>(spinner);
    }

    protected ComponentMatcher constructSliderMatcher(JSlider slider) {
        return new ComponentMatcher<>(slider);
    }

    protected ComponentMatcher constructProgressBarMatcher(JProgressBar progressBar) {
        return new ComponentMatcher<>(progressBar);
    }

    protected ComponentMatcher constructScrollBarMatcher(JScrollBar scrollBar) {
        return new ComponentMatcher<>(scrollBar);
    }
}

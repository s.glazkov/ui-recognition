package me.x3n.UIRecognition;

import com.bulenkov.darcula.DarculaLaf;
import me.x3n.UIRecognition.matchers.darcula.DarculaComponentMatcherFactory;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class RandomTestPipeline {
    private static final int NUM_TESTS = 50;
    private static final int GROUP_SIZE = 20;
    public static final double SCALE_FACTOR = 10.0;

    public static void main(String[] args) throws UnsupportedLookAndFeelException, IOException {
        UIManager.setLookAndFeel(new DarculaLaf());
        WindowValidator.setFactoryPrototype(new DarculaComponentMatcherFactory());

        double sumRecall = 0;
        ArrayList<Double> recalls = new ArrayList<>();

        for (int i = 1; i <= NUM_TESTS; i++) {
            double recall = evaluateGroup(GROUP_SIZE, 20, i);
            recalls.add(recall);
            sumRecall += recall;
            System.out.println(String.format("Recall on group %d: %.2f%%", i, recall * 100.0));
        }

        double avg = sumRecall / NUM_TESTS;
        double sumRecallSq = 0.0;
        for (double recall : recalls) {
            sumRecallSq += Math.pow(recall - avg, 2);
        }
        double stdev = Math.sqrt(sumRecallSq / (NUM_TESTS - 1));

        System.out.println(String.format("Avg. recall: %.2f%%", avg * 100.0));
        System.out.print(String.format("Recall stdev: %.2f%%", stdev * 100.0));
        LocalTesseract.endLocalTessBaseAPI();
    }

    private static double evaluateGroup(double groupSize, double formComplexity, int groupNo) throws IOException {
        int good = 0;
        int totalTerminals = 0;

        for (int i = 0; i < groupSize; i++) {
            RandomForm randomForm = new RandomForm(20);

            JFrame frame = new JFrame();
            frame.setContentPane(randomForm.getPanel());
            frame.pack();

            BufferedImage image = RenderUtils.renderWindow(frame);
            // BufferedImage hiResReference = RenderUtils.renderWindowScaled(frame, SCALE_FACTOR);
            ImageIO.write(image, "png", new File(System.getProperty("user.home"),
                    String.format("UIRecognition/%03d-%03d.png", groupNo, i)));
            // ImageIO.write(hiResReference, "png", new File(System.getProperty("user.home"),
            //          String.format("UIRecognition/%03d-%03d-hires.png", groupNo, i)));

            WindowValidator validator = new WindowValidator(frame);
            for (JComponent component : randomForm.getTerminals()) {
                validator.addComponentToValidate(component);
            }

            int numTerminals = randomForm.getTerminals().size();
            double recall = validator.validate(image); //, hiResReference, SCALE_FACTOR);
            int found = (int) Math.round(recall * numTerminals);

            BufferedImage out = validator.visualizeBestConfiguration();
            if (out != null) {
                ImageIO.write(out, "png", new File(System.getProperty("user.home"),
                        String.format("UIRecognition/%03d-%03d-out.png", groupNo, i)));
            }

            good += found;
            totalTerminals += numTerminals;

            frame.dispose();
        }

        return (double) good / totalTerminals;
    }
}

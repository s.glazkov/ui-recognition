package me.x3n.UIRecognition;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public final class WindowValidator {
    private static final int VISUALIZATION_MARGIN_PX = 5;
    private static final double EPS = 1e-6;

    private static ComponentMatcherFactory factoryPrototype = new SwingComponentMatcherFactory();
    private RootPaneContainer rootPaneContainer;
    private java.util.List<JComponent> componentsToValidate = new ArrayList<>();
    private ComponentMatcherFactory componentMatcherFactory;
    private BufferedImage lastImage = null;
    private Map<JComponent, Rectangle> bestConfiguration = null;

    public WindowValidator(RootPaneContainer rootPaneContainer) {
        if (!(rootPaneContainer instanceof Component)) {
            // This should never be thrown
            throw new IllegalArgumentException("Window is not an AWT component");
        }
        this.rootPaneContainer = rootPaneContainer;
        componentMatcherFactory = factoryPrototype.clone();
    }

    public static void setFactoryPrototype(ComponentMatcherFactory factoryPrototype) {
        WindowValidator.factoryPrototype = factoryPrototype;
    }

    public void addComponentToValidate(JComponent component) {
        componentsToValidate.add(component);
    }

    public void setComponentMatcherFactory(ComponentMatcherFactory componentMatcherFactory) {
        this.componentMatcherFactory = componentMatcherFactory;
    }

    public boolean validateFull(BufferedImage image) {
        return validateFull(image, null, -1);
    }

    public boolean validateFull(BufferedImage image, BufferedImage hiResReference, double scaleFactor) {
        return Math.abs(validate(image, hiResReference, scaleFactor) - 1.0) < EPS;
    }

    public double validate(BufferedImage image) {
        return validate(image, null, -1);
    }

    public double validate(BufferedImage image, BufferedImage hiResReference, double scaleFactor) {
        // To calculate IOU
        lastImage = image;
        rootPaneContainer.getRootPane().setSize(image.getWidth(), image.getHeight());

        ConfigurationFinder cf = new ConfigurationFinder(image, hiResReference, scaleFactor);
        double recall = cf.getRecall();
        bestConfiguration = cf.bestConfiguration;

        return recall;
    }

    public Map<JComponent, Rectangle> getBestConfiguration() {
        return bestConfiguration;
    }

    public BufferedImage visualizeBestConfiguration() {
        if (bestConfiguration == null) {
            return null;
        }

        JRootPane rootPane = rootPaneContainer.getRootPane();
        BufferedImage rootPaneImage = new BufferedImage(
                rootPane.getWidth(),
                rootPane.getHeight(),
                BufferedImage.TYPE_3BYTE_BGR
        );
        rootPane.paint(rootPaneImage.getGraphics());

        BufferedImage image = new BufferedImage(
                rootPane.getWidth() + lastImage.getWidth() + 3 * VISUALIZATION_MARGIN_PX,
                Math.max(rootPane.getHeight(), lastImage.getHeight()) + 2 * VISUALIZATION_MARGIN_PX,
                BufferedImage.TYPE_3BYTE_BGR
        );

        int leftOffsetX = VISUALIZATION_MARGIN_PX;
        int leftOffsetY = VISUALIZATION_MARGIN_PX;
        int rightOffsetX = rootPane.getWidth() + 2 * VISUALIZATION_MARGIN_PX;
        int rightOffsetY = VISUALIZATION_MARGIN_PX;

        Graphics2D g2d = image.createGraphics();
        g2d.drawImage(rootPaneImage, leftOffsetX, leftOffsetY, null);
        g2d.drawImage(lastImage, rightOffsetX, rightOffsetY, null);

        for (JComponent component : componentsToValidate) {
            float hue = ThreadLocalRandom.current().nextFloat();
            Color color = Color.getHSBColor(hue, 0.9f, 1.0f);

            Rectangle groundTruth = SwingUtilities.convertRectangle(
                    component.getParent(), component.getBounds(), rootPaneContainer.getContentPane());

            g2d.setColor(color);
            g2d.drawRect(groundTruth.x + leftOffsetX, groundTruth.y + leftOffsetY,
                    groundTruth.width, groundTruth.height);
            
            if (bestConfiguration.containsKey(component)) {
                Rectangle rect = bestConfiguration.get(component);
                if (rect.equals(new Rectangle(0, 0, 0, 0))) {
                    continue;
                }
                g2d.drawRect(rect.x + rightOffsetX, rect.y + rightOffsetY, rect.width, rect.height);
            }
        }

        return image;
    }

    class ConfigurationFinder {
        private double scaleFactor;
        private BufferedImage hiResReference;
        private BufferedImage image;
        private LinkedHashMap<JComponent, List<Rectangle>> foundBoundingBoxes = new LinkedHashMap<>();
        private LinkedHashMap<JComponent, Rectangle> assignedBoundingBoxes = new LinkedHashMap<>();
        private HashMap<JComponent, Rectangle> bestConfiguration = new HashMap<>();
        private ArrayList<Rectangle> used = new ArrayList<>();
        private int maxFoundSoFar = 0;
        private double maxScoreSoFar = 0;

        ConfigurationFinder(BufferedImage image, BufferedImage hiResReference, double scaleFactor) {
            this.image = image;
            this.hiResReference = hiResReference;
            this.scaleFactor = scaleFactor;
        }

        double getRecall() {
            traverse((JComponent) rootPaneContainer.getRootPane().getContentPane());
            return getMaxRecall(0);
        }

        private void traverse(JComponent component) {
            if (component instanceof JPanel) {
                for (Component child : component.getComponents()) {
                    if (!(child instanceof JComponent)) {
                        throw new RuntimeException("Encountered a non-Swing component while traversing component tree");
                    }
                    traverse((JComponent) child);
                }
            }
            else if (componentsToValidate.contains(component)) {
                findBoundingBoxes(component);
            }
        }

        private double getMaxRecall(int i) {
            if (i >= componentsToValidate.size()) {
                return getRecallForCurrentConfiguration();
            }
            double score = 0.0;
            JComponent currentComponent = componentsToValidate.get(i);

            boolean noBoxes = true;
            for (Rectangle bb : foundBoundingBoxes.get(currentComponent)) {
                if (isUsed(bb)) {
                    continue;
                }
                noBoxes = false;
                assignedBoundingBoxes.put(currentComponent, bb);
                used.add(bb);
                score = Math.max(score, getMaxRecall(i + 1));
                assignedBoundingBoxes.remove(currentComponent);
                used.remove(bb);
            }

            if (noBoxes) {
                // IoU would be zero
                assignedBoundingBoxes.put(currentComponent, new Rectangle(0, 0, 0, 0));
                score = Math.max(score, getMaxRecall(i + 1));
                assignedBoundingBoxes.remove(currentComponent);
            }

            return score;
        }

        private double getRecallForCurrentConfiguration() {
            int found = 0;
            double score = 0.0;
            for (Map.Entry<JComponent, Rectangle> entry : assignedBoundingBoxes.entrySet()) {
                JComponent component = entry.getKey();
                Rectangle groundTruth = SwingUtilities.convertRectangle(
                        component.getParent(), component.getBounds(), rootPaneContainer.getContentPane());
                if (!entry.getValue().equals(new Rectangle(0, 0, 0, 0))) {
                    found++;
                    score += calculateIoU(groundTruth, entry.getValue());
                }
            }
            score /= found;
            if (found > maxFoundSoFar || (found == maxFoundSoFar && score > maxScoreSoFar)) {
                maxFoundSoFar = found;
                maxScoreSoFar = score;
                bestConfiguration.clear();
                for (Map.Entry<JComponent, Rectangle> entry : assignedBoundingBoxes.entrySet()) {
                    if (entry.getValue().equals(new Rectangle(0, 0, 0, 0))) {
                        continue;
                    }
                    bestConfiguration.put(entry.getKey(), entry.getValue());
                }
            }
            return (double) found / componentsToValidate.size();
        }

        private boolean isUsed(Rectangle bb) {
            for (Rectangle rect : used) {
                if (calculateOverlap(bb, rect) > 0.5) {
                    return false;
                }
            }
            return false;
        }

        private List<Rectangle> findBoundingBoxes(JComponent component) {
            if (!foundBoundingBoxes.containsKey(component)) {
                ComponentMatcher matcher = componentMatcherFactory.constructMatcher(component);
                foundBoundingBoxes.put(component, matcher.getPossibleBoundingBoxes(image, hiResReference, scaleFactor));
            }
            return foundBoundingBoxes.get(component);
        }
    }

    private static double calculateIoU(Rectangle rect1, Rectangle rect2) {
        long interArea = getInterArea(rect1, rect2);
        long rect1Area = rect1.width * rect1.height;
        long rect2Area = rect2.width * rect2.height;

        return (double) interArea / (rect1Area + rect2Area - interArea);
    }

    private static double calculateOverlap(Rectangle rect1, Rectangle rect2) {
        long interArea = getInterArea(rect1, rect2);
        long rect1Area = rect1.width * rect1.height;
        long rect2Area = rect2.width * rect2.height;

        return Math.max((double) interArea / rect1Area, (double) interArea / rect2Area);
    }

    private static long getInterArea(Rectangle rect1, Rectangle rect2) {
        int x1 = rect1.x, y1 = rect1.y;
        int x2 = rect1.x + rect1.width - 1, y2 = rect1.y + rect1.height - 1;
        int x3 = rect2.x, y3 = rect2.y;
        int x4 = rect2.x + rect2.width - 1, y4 = rect2.y + rect2.height - 1;

        int xA = Math.max(x1, x3);
        int yA = Math.max(y1, y3);
        int xB = Math.min(x2, x4);
        int yB = Math.min(y2, y4);

        return (long) Math.max(0, xB - xA + 1) * Math.max(0, yB - yA + 1);
    }
}

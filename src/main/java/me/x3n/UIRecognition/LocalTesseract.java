package me.x3n.UIRecognition;

import org.bytedeco.javacpp.BytePointer;

import javax.imageio.*;
import javax.imageio.metadata.IIOInvalidTreeException;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;

import static org.bytedeco.javacpp.lept.*;
import static org.bytedeco.javacpp.tesseract.*;

public class LocalTesseract {
    private static final double MM_PER_INCH = 25.4;
    private static final double DPI = 300.0;
    private static TessBaseAPI api;

    static {
        api = new TessBaseAPI();
        if (api.Init(null, "ENG") != 0) {
            throw new RuntimeException("Couldn't initialize tesseract");
        }
    }

    public static String detectText(BufferedImage image) {
        byte[] data = LocalTesseract.bufferedImageToPngWithDPI(image, DPI);
        PIX pix = pixReadMemPng(data, data.length);
        api.SetImage(pix);

        BytePointer outText = api.GetUTF8Text();
        String text = outText.getString().trim();

        pixDestroy(pix);
        outText.deallocate();
        // System.out.println(text);

        return text;
    }

    public static TessBaseAPI getLocalTessBaseAPI() {
        return api;
    }

    public static void endLocalTessBaseAPI() {
        api.End();
    }

    private static void setDPI(IIOMetadata metadata, double dpi) {
        double dotsPerMilli = dpi / MM_PER_INCH;

        IIOMetadataNode horiz = new IIOMetadataNode("HorizontalPixelSize");
        horiz.setAttribute("value", Double.toString(dotsPerMilli));

        IIOMetadataNode vert = new IIOMetadataNode("VerticalPixelSize");
        vert.setAttribute("value", Double.toString(dotsPerMilli));

        IIOMetadataNode dim = new IIOMetadataNode("Dimension");
        dim.appendChild(horiz);
        dim.appendChild(vert);

        IIOMetadataNode root = new IIOMetadataNode("javax_imageio_1.0");
        root.appendChild(dim);

        try {
            metadata.mergeTree("javax_imageio_1.0", root);
        }
        catch (IIOInvalidTreeException e) {
            throw new RuntimeException("Failed to modify metadata tree");
        }
    }

    public static byte[] bufferedImageToPngWithDPI(BufferedImage image, double dpi) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        for (Iterator<ImageWriter> iw = ImageIO.getImageWritersByFormatName("png"); iw.hasNext();) {
            ImageWriter writer = iw.next();

            ImageWriteParam writeParam = writer.getDefaultWriteParam();
            ImageTypeSpecifier typeSpecifier = ImageTypeSpecifier.createFromBufferedImageType(BufferedImage.TYPE_INT_RGB);
            IIOMetadata metadata = writer.getDefaultImageMetadata(typeSpecifier, writeParam);
            if (metadata.isReadOnly() || !metadata.isStandardMetadataFormatSupported()) {
                continue;
            }

            setDPI(metadata, dpi);

            try (ImageOutputStream stream = ImageIO.createImageOutputStream(baos)) {
                writer.setOutput(stream);
                writer.write(metadata, new IIOImage(image, null, metadata), writeParam);
            }
            catch (IOException e) {
                throw new RuntimeException("Failed to write PNG to stream");
            }
            break;
        }

        return baos.toByteArray();
    }
}

package me.x3n.UIRecognition;

import org.bytedeco.javacv.Java2DFrameUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;

public class ComponentMatcher<T extends JComponent> {
    private static final double DEFAULT_SCALE_FACTOR = 10.0;
    protected T component;

    public ComponentMatcher(T component) {
        this.component = component;
    }

    public final java.util.List<Rectangle> getPossibleBoundingBoxes(BufferedImage image) {
        return getPossibleBoundingBoxes(image, null, DEFAULT_SCALE_FACTOR);
    }

    public List<Rectangle> getPossibleBoundingBoxes(
            BufferedImage image, BufferedImage hiResReference, double scaleFactor)
    {
        if (scaleFactor < 0) {
            scaleFactor = DEFAULT_SCALE_FACTOR;
        }
        if (hiResReference == null) {
            Mat img = Java2DFrameUtils.toMat(image);
            Mat reference = new Mat();
            bitwise_not(img, img);
            resize(img, reference,
                    new Size((int) (img.cols() * scaleFactor), (int) (img.rows() * scaleFactor)),
                    0, 0, INTER_LANCZOS4);
            bitwise_not(reference, reference);
            hiResReference = Java2DFrameUtils.toBufferedImage(reference);
        }
        return doGetPossibleBoundingBoxes(image, hiResReference, scaleFactor);
    }

    protected List<Rectangle> doGetPossibleBoundingBoxes(
            BufferedImage image, BufferedImage hiResReference, double scaleFactor)
    {
        return new ArrayList<>();
    }
}

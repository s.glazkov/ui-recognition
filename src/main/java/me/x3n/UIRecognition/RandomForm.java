package me.x3n.UIRecognition;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class RandomForm {
    private static final String WORDS_FILE = "./src/main/resources/words.txt";
    private static ArrayList<String> FONTS = new ArrayList<>(
            Arrays.asList("serif", "sansserif", "monospaced"));
    private static ArrayList<String> words = new ArrayList<>();
    private static Random random = new Random(1337);

    static {
        try (BufferedReader br = new BufferedReader(new FileReader(WORDS_FILE))) {
            String line;
            while ((line = br.readLine()) != null) {
                words.add(line);
            }
        }
        catch (Exception e) {
            words.add("Text");
        }
    }

    private JPanel panel = new JPanel();
    private ArrayList<ArrayList<Integer>> adjacencyList = new ArrayList<>();
    private java.util.List<JComponent> terminals = new ArrayList<>();

    public RandomForm(int complexity) {
        generateTree(complexity);

        panel.add(traverse(0, -1));
        panel.setLayout(getRandomLayout(panel));
    }

    static JComponent getRandomComponent() {
        String fontName = FONTS.get(random.nextInt(FONTS.size()));
        int fontType = Font.PLAIN;
        if (random.nextBoolean()) {
            fontType |= Font.BOLD;
        }
        if (random.nextBoolean()) {
            fontType |= Font.ITALIC;
        }
        int fontSize = random.nextInt(12) + 12;
        Font font = new Font(fontName, fontType, fontSize);

        String s = words.get(random.nextInt(words.size()));

        int x = random.nextInt(2);
        if (x == 0) {
            JButton button = new JButton(s);
            button.setFont(font);
            return button;
        }
        else {
            JLabel label = new JLabel(s);
            label.setFont(font);
            label.setBorder(new EmptyBorder(0, 5, 0, 5)); // For text segmentation to work right
            return label;
        }
    }

    static LayoutManager getRandomLayout(JPanel panel) {
        int x = random.nextInt(4);
        if (x == 0) {
            return new BoxLayout(panel, BoxLayout.X_AXIS);
        }
        else if (x == 1) {
            return new BoxLayout(panel, BoxLayout.Y_AXIS);
        }
        else if (x == 2) {
            int childrenCount = panel.getComponents().length;
            int rows = getRandomDivisor(childrenCount);
            return new GridLayout(rows, childrenCount / rows);
        }
        return new FlowLayout();
    }

    private static int getRandomDivisor(int n) {
        ArrayList<Integer> divisors = new ArrayList<>();
        for (int i = 1; i * i <= n; i++) {
            if (n % i == 0) {
                divisors.add(i);
                if (i * i != n) {
                    divisors.add(n / i);
                }
            }
        }

        int idx = random.nextInt(divisors.size());
        return divisors.get(idx);
    }

    public JPanel getPanel() {
        return panel;
    }

    public java.util.List<JComponent> getTerminals() {
        return terminals;
    }

    private JComponent traverse(int u, int from) {
        int childrenCount = adjacencyList.get(u).size() - (from != -1 ? 1 : 0);

        if (childrenCount == 0) {
            JComponent component = getRandomComponent();
            terminals.add(component);
            return component;
        }
        if (childrenCount == 1) {
            for (int v : adjacencyList.get(u)) {
                if (v == from) {
                    continue;
                }
                return traverse(v, u);
            }
        }

        JPanel p = new JPanel();
        for (int v : adjacencyList.get(u)) {
            if (v == from) {
                continue;
            }
            p.add(traverse(v, u));
        }
        p.setLayout(getRandomLayout(p));

        return p;
    }

    private void generateTree(int n) {
        int[] prueferCode = new int[n - 2];
        int[] degree = new int[n];

        for (int i = 0; i < n - 2; i++) {
            prueferCode[i] = random.nextInt(n);
        }
        adjacencyList.clear();
        for (int i = 0; i < n; i++) {
            adjacencyList.add(new ArrayList<>());
            degree[i] = 1;
        }
        for (int i = 0; i < n - 2; i++) {
            degree[prueferCode[i]]++;
        }

        for (int i = 0; i < n - 2; i++) {
            int u = prueferCode[i];
            for (int j = 0; j < n; j++) {
                if (degree[j] == 1) {
                    adjacencyList.get(u).add(j);
                    adjacencyList.get(j).add(u);
                    degree[u]--;
                    degree[j]--;
                    break;
                }
            }
        }

        int u = -1, v = -1;
        for (int i = 0; i < n; i++) {
            if (degree[i] == 1) {
                if (u == -1) {
                    u = i;
                }
                else {
                    v = i;
                    break;
                }
            }
        }
        adjacencyList.get(u).add(v);
        adjacencyList.get(v).add(u);
    }
}

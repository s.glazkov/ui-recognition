package me.x3n.UIRecognition;

public interface ValidatableComponent {
    ComponentMatcher constructComponentFinder();
}

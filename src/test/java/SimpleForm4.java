import javax.swing.*;
import java.util.Arrays;
import java.util.List;

public class SimpleForm4 implements ValidatableForm {
    public JPanel getPanel() {
        return panel;
    }
    public List<JComponent> getComponentsToValidate() {
        return Arrays.asList(connectButton, sendMessageButton, userLabel);
    }

    private JPanel panel;
    private JTextField userTextField;
    private JButton connectButton;
    private JFormattedTextField writeFormattedTextField;
    private JButton sendMessageButton;
    private JLabel userLabel;
    private JTextPane chatPane;
}

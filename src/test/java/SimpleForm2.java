import javax.swing.*;
import java.util.Arrays;
import java.util.List;

public class SimpleForm2 implements ValidatableForm {
    private JTextField tabSizeTextField;
    private JCheckBox useTabsInsteadOfCheckBox;
    private JPanel panel;
    private JButton applyButton;
    private JButton cancelButton;
    private JButton okButton;
    private JLabel headerLabel;
    private JLabel tabSizeLabel;

    public JPanel getPanel() {
        return panel;
    }
    public List<JComponent> getComponentsToValidate() {
        return Arrays.asList(useTabsInsteadOfCheckBox, applyButton, cancelButton, okButton, headerLabel, tabSizeLabel);
    }
}

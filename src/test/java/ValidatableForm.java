import javax.swing.*;
import java.util.List;

public interface ValidatableForm {
    JPanel getPanel();
    List<JComponent> getComponentsToValidate();
}

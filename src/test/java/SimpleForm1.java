import javax.swing.*;
import java.util.Arrays;
import java.util.List;

public class SimpleForm1 implements ValidatableForm {
    private JPanel panel;
    private JTextField loginField;
    private JPasswordField passwordField;
    private JButton cancelButton;
    private JButton okButton;
    private JLabel loginLabel;
    private JLabel passwordLabel;

    public JPanel getPanel() {
        return panel;
    }
    public List<JComponent> getComponentsToValidate() {
        return Arrays.asList(loginLabel, passwordLabel, okButton, cancelButton);
    }
}

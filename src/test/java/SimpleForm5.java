import javax.swing.*;
import java.util.Arrays;
import java.util.List;

public class SimpleForm5 implements ValidatableForm {
    public JPanel getPanel() {
        return panel;
    }
    public List<JComponent> getComponentsToValidate() {
        return Arrays.asList(areYouSureLabel, yesButton, noButton);
    }

    private JButton yesButton;
    private JButton noButton;
    private JPanel panel;
    private JLabel areYouSureLabel;
}
